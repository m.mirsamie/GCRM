function isCopyPressed(e) {
    return e.ctrlKey && getEventKeyCode(e) == 99;
}
function isPastePressed(e) {
    return e.ctrlKey && getEventKeyCode(e) == 118;
}
function getEventKeyCode(e) {
    var key;
    if (window.event)
        key = event.keyCode;
    else
        key = e.which;

    return key;
}
function ignoreKeys(key) {
    if (key == 0) { //function keys and arrow keys
        return true;
    }
    if (key == 13) { //return
        return true;
    }
    if (key == 8) { //backspace
        return true;
    }
    if (key == 9) { // tab
        return true;
    }

    return false;
}
function numbericOnKeypress(e) {

    if (isCopyPressed(e) || isPastePressed(e))
        return;
    var key = getEventKeyCode(e);

    if (key == 45 || key == 44 || key == 46) return true;// ',' '.' '-'
    if (ignoreKeys(key)) return true;
    if (isNumericKeysPressed(key)) { // Numbers
        return true;
    }
    if (window.event) //IE
        window.event.returnValue = false;
    else              //other browser
        e.preventDefault();
}
function isNumericKeysPressed(key) {

    if (key >= 48 && key <= 57) { // Numbers
        return true;
    }

    return false;
}
