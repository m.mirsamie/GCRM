package com.gcom.gcrm;

import java.util.ArrayList;
import java.util.List;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;


public class productData {
	private Context mContext;
	private SQLiteDatabase database;
	private msqlite dbHelper;
	private String[] allColumns = {"id","name","pic"};
	public productData(Context context)
	{
		this.mContext = context;
		dbHelper = new msqlite(context);
	}
	public void open() throws SQLException {
		try
		{
			database = dbHelper.getWritableDatabase();
		}catch (Exception e) {
			Toast.makeText(this.mContext, e.getMessage(), Toast.LENGTH_LONG).show();
		}
	}
	public void close() {
		dbHelper.close();
	}
	public void addProduct(int id, String name,String pic) {
		ContentValues values = new ContentValues();
		values.put("id", id);
		values.put("name", name);
		values.put("pic", pic);
		database.insert(msqlite.PRODUCT_TABLE_NAME, null,values);
	}
	public void addProducts(String txt) {
		//String query = "insert into "+msqlite.PRODUCT_TABLE_NAME+" ";
       	String[] cells = txt.split(",");
       	try {
       		database.beginTransaction();
        	for(int i = 0;i<cells.length;i++)
        	{
        		String tmp = cells[i].toString();
        		String[] fields = tmp.split("[|]");
        		int id = Integer.valueOf(fields[0]);
        		String name = fields[1];
        		String pic = fields.length==3?fields[2]:"";
        		addProduct(id,name,pic);
        	}
        	database.setTransactionSuccessful();
		} catch (Exception e) {
			// TODO: handle exception
		}
       	finally{
       		database.endTransaction();
       	}
       	
	}
	public List<product> getList()
	{
		List<product> out = new ArrayList<product>();
		try {
			Cursor cursor = database.query(msqlite.PRODUCT_TABLE_NAME,allColumns, null, null, null, null, "name");
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				product newpr = cursorToProduct(cursor);
				out.add(newpr);
				cursor.moveToNext();
			}
			cursor.close();
		} catch (Exception e) {
			Toast.makeText(mContext, "DB error "+e.getMessage(), Toast.LENGTH_SHORT).show();
		}
		return out;
	}
	  public product getProduct(int id)
	  {
		  product out = new product();
		  Cursor cursor = database.query(msqlite.PRODUCT_TABLE_NAME,allColumns, " id = "+id+" ", null, null, null, null, null);
		  cursor.moveToFirst();
		  if(!cursor.isAfterLast())
		  {
			  out = cursorToProduct(cursor);
		  }
		  cursor.close();
		  return out;
	  }
	  public void emptyTable()
	  {
		  database.execSQL("delete from "+msqlite.PRODUCT_TABLE_NAME+";");
		  //database.delete(msqlite.PRODUCT_TABLE_NAME, null, null);
		  //dbHelper.onCreate(database);
		  
	  }
	  public product cursorToProduct(Cursor cursor)
	  {
		  product pr = new product();
		  int id = (int) cursor.getLong(0);
		  String name = cursor.getString(1);
		  String pic = cursor.getString(2);
		  pr.set(id, name, pic);
		  return pr;
	  }
}
