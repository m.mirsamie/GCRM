package com.gcom.gcrm;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

public class order_tbData {
	  private Context mContext;
	  private SQLiteDatabase database;
	  private msqlite dbHelper;
	  private String[] allColumns = {"id","tarikh"};
	  public order_tbData(Context context)
	  {
		  this.mContext = context;
		  dbHelper = new msqlite(context);
	  }
	  public void open() throws SQLException {
		  try
		  {
			  database = dbHelper.getWritableDatabase();
		  }catch (Exception e) {
			// TODO: handle exception
			  Toast.makeText(this.mContext, e.getMessage(), Toast.LENGTH_LONG).show();
		  }
	  }
	  public void close() {
		  dbHelper.close();
	  }
	  public int addOrder(String tarikh)
	  {
		  ContentValues values = new ContentValues();
		  int id = getLastId()+1;
		  values.put("id", id);
		  values.put("tarikh", tarikh);
		  database.insert(msqlite.OREDER_TABLE_NAME, null,values);
		  return(id);
	  }
	  public int addOrder()
	  {
		  ContentValues values = new ContentValues();
		  int id = getLastId()+1;
		  values.put("id", id);
		  values.put("tarikh",WebAppInterface.getToday());
		  database.insert(msqlite.OREDER_TABLE_NAME, null,values);
		  return(id);
	  }
	  public int getLastId()
	  {
		  Cursor cur = database.rawQuery("select MAX(id) lid from "+msqlite.OREDER_TABLE_NAME, null);
		  int lid;
		  if (cur.moveToFirst()) 
			    lid = cur.getInt(cur.getColumnIndex("lid"));
		  else 
				lid=0;
		  return(lid);
	  }
	  public List<order_tb> getAll()
	  {
		  List<order_tb> out = new ArrayList<order_tb>();
		  try {
				Cursor cursor = database.query(msqlite.OREDER_TABLE_NAME,allColumns, null, null, null, null, "id");
				cursor.moveToFirst();
				while (!cursor.isAfterLast()) {
					order_tb newpr = cursorToOrder(cursor);
					out.add(newpr);
					cursor.moveToNext();
				}
				cursor.close();
			} catch (Exception e) {
				Toast.makeText(mContext, "DB error "+e.getMessage(), Toast.LENGTH_SHORT).show();
				/*
				try {
					
					database.execSQL("create table " +msqlite.OREDER_TABLE_NAME + " (id integer primary key,tarikh text);");
				} catch (Exception e2) {
					Toast.makeText(mContext, "DB error "+e.getMessage(), Toast.LENGTH_SHORT).show();
				}
				*/
			}
		  return(out);
	  }
	  public order_tb cursorToOrder(Cursor cursor)
	  {
		  int id = (int) cursor.getLong(0);
		  String tarikh = cursor.getString(1);
		  order_tb ord = new order_tb(id,tarikh);
		  return ord;
	  }
}
