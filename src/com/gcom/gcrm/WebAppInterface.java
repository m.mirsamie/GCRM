package com.gcom.gcrm;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.PowerManager;
import android.telephony.SmsManager;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

public class WebAppInterface {
    Context mContext;
    productData prdb;
    String ajaxout="";
    String javaScriptFn = "";
    Boolean ajaxReady = true;
    int ajaxstartSec=0,ajaxendSec = 0;
    Conf conf;

    WebAppInterface(Context c) {
        mContext = c;
        prdb = new productData(mContext);
		prdb.open();
		conf = new Conf(mContext);
		conf.open();
    }

    @JavascriptInterface
    public void showToast(String toast) {
        Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
    }
    @JavascriptInterface
    public String getText()
    {
		String out = "";
		List<product> pr = prdb.getList();
		for(int i = 0;i < pr.size();i++){
			out += ((out != "")?",":"")+pr.get(i).id+"|"+pr.get(i).name+"|"+pr.get(i).pic;
		}
    	return(out);
    }
    @JavascriptInterface
    public void putText(String txt)
    {
    	try
    	{
	    	prdb.emptyTable();
	    	/*
	       	String[] cells = txt.split(",");
	    	for(int i = 0;i<cells.length;i++)
	    	{
	    		String tmp = cells[i].toString();
	    		String[] fields = tmp.split("[|]");
	    		int id = Integer.valueOf(fields[0]);
	    		String name = fields[1];
	    		String pic = fields.length==3?fields[2]:"";
	    		prdb.addProduct(id , name, pic);
	    	}
	    	*/
	    	prdb.addProducts(txt);
    	}
    	catch (Exception e) {
			// TODO: handle exception
    		Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_LONG).show();
		}
    }
    @JavascriptInterface
    public void putConf(String txt)
    {
    	try
    	{
	    	conf.emptyTable();
	       	String[] cells = txt.split(",");
	    	for(int i = 0;i<cells.length;i++)
	    	{
	    		String tmp = cells[i].toString();
	    		String[] fields = tmp.split("[|]");    		
	    		String key = fields[0];
	    		String value = fields.length==2?fields[1]:"";
	    		conf.addConf(key, value);
	    	}
    	}catch (Exception e) {
    		Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_LONG).show();
		}
    }
    @JavascriptInterface
	public String hasInternet()
    {
    	Boolean isInternetPresent = false;
    	try
        {
	        ConnectionDetector cd = new ConnectionDetector(mContext);    	    	 
	    	isInternetPresent = cd.isConnectingToInternet();
        }
        catch(Exception e)
        {
        }
    	String out = isInternetPresent?"true":"false";
    	return(out);
    }
    @SuppressLint("UnlocalizedSms")
    @JavascriptInterface
	public void sendSms(String myMessage)
    {
    	
    	try
    	{

    		//String smsCenter = "09153068145";
	    	String smsCenter = "30008910488565";
    		
	    	ConfDet smsConf = conf.getConf("smsCenter");
	    	if(smsConf.id > 0 && smsConf.value != "")
	    	{
	    		smsCenter = smsConf.value;
	    	}
	    	
			SmsManager smsManager = SmsManager.getDefault();
			ArrayList<String> messageArray = smsManager.divideMessage(myMessage);
	    	ArrayList<PendingIntent> sentIntents = new ArrayList<PendingIntent>();
	    	smsManager.sendMultipartTextMessage(smsCenter,null,messageArray,sentIntents, null);
	    	//Toast.makeText(mContext, "Sent to "+smsCenter, Toast.LENGTH_LONG).show();
    	}
    	catch (Exception e) {
			// TODO: handle exception
    		Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_LONG).show();
		}
    	
    	//String phoneNumber="09153068145";
//    	String message=myMessage;
//        String SENT = "SMS_SENT";
//        String DELIVERED = "SMS_DELIVERED";
 
//        PendingIntent sentPI = PendingIntent.getBroadcast(mContext, 0,
//            new Intent(SENT), 0);
 
//        PendingIntent deliveredPI = PendingIntent.getBroadcast(mContext, 0,
//            new Intent(DELIVERED), 0);
 
        //---when the SMS has been sent---
        /*
        mContext.registerReceiver(new BroadcastReceiver(){
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode())
                {
                    case Activity.RESULT_OK:
                        Toast.makeText(mContext, "اطلاعات از طریق پیامک ارسال گردید", 
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(mContext, "خطای عمومی", 
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(mContext, "امکان ارسال نیست", 
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        Toast.makeText(mContext, "خطای مخابراتی", 
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(mContext, "به شبکه تلفن همراه متصل نیستید", 
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(SENT));
 
        //---when the SMS has been delivered---
        mContext.registerReceiver(new BroadcastReceiver(){
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode())
                {
                    case Activity.RESULT_OK:
                        Toast.makeText(mContext, "اطلاعات از طریق پیامک برای سرور ارسال گردید", 
                                Toast.LENGTH_SHORT).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(mContext, "متاسفانه اطلاعات از طریق پیامک ارسال نشد", 
                                Toast.LENGTH_SHORT).show();
                        break;                        
                }
            }
        }, new IntentFilter(DELIVERED));        

        SmsManager sms = SmsManager.getDefault();
		ArrayList<String> messageArray = sms.divideMessage(myMessage);
    	ArrayList<PendingIntent> sentIntents = new ArrayList<PendingIntent>();
    	ArrayList<PendingIntent> deliverIntents = new ArrayList<PendingIntent>();
        //sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
        sms.sendMultipartTextMessage(phoneNumber,null,messageArray,sentIntents, deliverIntents);
 		*/
   }
/*
    private void registerReceiver(BroadcastReceiver broadcastReceiver,
			IntentFilter intentFilter) {
		// TODO Auto-generated method stub
		
	}
*/
    
    //-----------------------------------------------------ORDER------------------------------------
    public static String getToday()
    {
    	Calendar cal = Calendar.getInstance();
    	String dt = String.valueOf(cal.get(Calendar.YEAR))+"-"+String.valueOf(cal.get(Calendar.MONTH))+"-"+String.valueOf(cal.get(Calendar.DAY_OF_MONTH))
    			+" "+String.valueOf(cal.get(Calendar.HOUR_OF_DAY))+":"+String.valueOf(cal.get(Calendar.MINUTE))+":"+String.valueOf(cal.get(Calendar.SECOND));
    	return dt;
    }
    @JavascriptInterface
    public String addOrder()
    {
    	order_tbData otd = new order_tbData(mContext);
    	otd.open();
    	String dt = WebAppInterface.getToday();
    	String oid = String.valueOf(otd.addOrder(dt));
    	otd.close();
    	return oid;
    }
    @JavascriptInterface
    public String addToOrder(int orderId,int productId,int tedad)
    {
    	OrderDetData odd = new OrderDetData(mContext);
    	odd.open();
    	String out = (odd.addOrder(orderId, productId,tedad)>0)?"true":"false";
    	odd.close();
    	return out;
    }
    @JavascriptInterface
    public String getOrders()
    {
    	String out = "[";
    	order_tbData otd = new order_tbData(mContext);
    	otd.open();
    	List<order_tb> orders = otd.getAll();
    	for(int i = 0;i < orders.size();i++)
    		out += ((out!="[")?",":"")+orders.get(i).toJSON();
    	out += "]";
    	otd.close();
    	return out;
    }
    @JavascriptInterface
    public String getOrderDetails(int orderId)
    {
    	String out = "[]";
    	return out;
    }
    //----------------------------------------------------------------------------------------------
    @JavascriptInterface
	public String getConf(String key)
    {
    	String out = "";
    	try
    	{
    		ConfDet con = conf.getConf(key);
    		out = con.value;
    	}catch (Exception e) {
			// TODO: handle exception
    		Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_LONG).show();
		}
    	return out;
    }
    @JavascriptInterface
    public String getAllConf()
    {
		String out = "";
		List<ConfDet> cn = conf.getList();
		for(int i = 0;i < cn.size();i++){
			out += ((out != "")?",":"")+cn.get(i).key+"|"+cn.get(i).value;
		}
    	return(out);
    }
    @JavascriptInterface
    public void upgrade()
    {
    	if(!Main.newVersion.equals(""))
    	{
	        Intent intent = new Intent(Intent.ACTION_VIEW);
	        intent.setDataAndType(Uri.fromFile(new File(Environment.getExternalStorageDirectory() + "/GCRM_update.apk")), "application/vnd.android.package-archive");
	        mContext.startActivity(intent);
    	}
    	else
    		showToast("نسخه جدیدی برای بروزآوری موجود نیست");
    }
    @JavascriptInterface
    public void khorooj()
    {
    	System.exit(0);
    }
    //-------------------------AJAX--------------------------------------------------
    @JavascriptInterface
    public String getAjax(String url,String jscript)
    {
    	String out = "false";
    	final ajax aj = new ajax(mContext);
    	javaScriptFn = jscript;
    	//showToast("ajax to :\n"+url);
    	aj.execute(url);
    	out = "true";
    	return out;
    }
    @JavascriptInterface
    public String ajaxResponse()
    {
    	if(ajaxReady)
    		return ajaxout;
    	else
    		return "NaN";
    }
    
	private class ajax extends AsyncTask<String, Integer, String> {

	    private Context context;

	    public ajax(Context context) {
	        this.context = context;
	    }

	    @SuppressLint({ "SdCardPath", "Wakelock" })
		@Override
	    protected String doInBackground(String... sUrl) {
	    	String out = "";
	        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
	        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
	             getClass().getName());
	        wl.acquire();
	        try {
	        	ajaxout = "";
	            InputStream input = null;
	            HttpURLConnection connection = null;
	            try {
	                URL url = new URL(sUrl[0]);
	                connection = (HttpURLConnection) url.openConnection();
	                connection.connect();
	                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK)
	                {
	                     out = "Server returned HTTP " + connection.getResponseCode() 
	                         + " " + connection.getResponseMessage();
	                     return out;
	                }
	                input = connection.getInputStream();
	                BufferedReader in = new BufferedReader(new InputStreamReader(input));
	                String line = null;
	                while ((line = in.readLine()) != null) {
	                	ajaxout += line;
	                }
	                in.close();
	            } catch (Exception e) {
	                return e.toString();
	            } finally {
	                try {
	                    if (input != null)
	                        input.close();
	                } 
	                catch (IOException ignored) { }

	                if (connection != null)
	                    connection.disconnect();
	            }
	        } finally {
	            wl.release();
	        }
	        return null;
	    }
	    @Override
	    protected void onPostExecute(String result) {
	        if (result == null)
	        {
				try {
					ajaxReady = true;
					Main ma = (Main) mContext;
					if(javaScriptFn != "")
						ma.mywebview.loadUrl("javascript:"+javaScriptFn+"('"+ajaxout+"');");
					//showToast("Done ajax:\n"+"javascript:"+javaScriptFn+"('"+ajaxout+"');");
				} catch (Exception e) {
				}
	        }
	    }
	}
}