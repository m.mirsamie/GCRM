package com.gcom.gcrm;


import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;

public class Into extends Activity {
	private Integer SPLASH_DISPLAY_LENGHT = 2000;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_into);
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
            	Intent intent = new Intent(Into.this,Main.class);
    	        //intent.putExtra(EXTRA_MESSAGE, message);	
    	        startActivity(intent);
    	        Into.this.finish();
                //Intent mainIntent = new Intent(Splash.this,Menu.class);
                //Splash.this.startActivity(mainIntent);
                //Splash.this.finish();
            }
        }, SPLASH_DISPLAY_LENGHT);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.into, menu);
		return true;
	}

}
