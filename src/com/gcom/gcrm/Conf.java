package com.gcom.gcrm;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

public class Conf {
	  // Database fields
	  private Context mContext;
	  private SQLiteDatabase database;
	  private msqlite dbHelper;
	  private String[] allColumns = {"id","cKey","cValue"};
	  public Conf(Context context)
	  {
		  this.mContext = context;
		  dbHelper = new msqlite(context);
	  }
	  public void open() throws SQLException {
		  try
		  {
			  database = dbHelper.getWritableDatabase();
		  }catch (Exception e) {
			// TODO: handle exception
			  Toast.makeText(this.mContext, e.getMessage(), Toast.LENGTH_LONG).show();
		  }
	  }
	  public void close() {
		  dbHelper.close();
	  }
	  public void addConf(String cKey,String cValue) {
		  ContentValues values = new ContentValues();
		  values.put("cKey", cKey);
		  values.put("cValue", cValue);
		  database.insert(msqlite.CONF_TABLE_NAME, null,values);
	  }
	  public List<ConfDet> getList()
	  {
		  List<ConfDet> out = new ArrayList<ConfDet>();
		  Cursor cursor = database.query(msqlite.CONF_TABLE_NAME,allColumns, null, null, null, null, "id");
		  cursor.moveToFirst();
		  while (!cursor.isAfterLast()) {
			  ConfDet newpr = cursorToConf(cursor);
			  out.add(newpr);
			  cursor.moveToNext();
		  }
		  cursor.close();
		  return out;
	  }
	  public ConfDet getConf(String key)
	  {
		  ConfDet out = new ConfDet(-1,null,null);
		  Cursor cursor = database.query(msqlite.CONF_TABLE_NAME,allColumns, " cKey = '"+key+"' ", null, null, null, null, null);
		  cursor.moveToFirst();
		  if(!cursor.isAfterLast())
		  {
			  out = cursorToConf(cursor);
		  }
		  cursor.close();
		  return out;
	  }
	  public void emptyTable()
	  {
		  database.execSQL("delete from "+msqlite.CONF_TABLE_NAME+";");
		  //database.delete(msqlite.PRODUCT_TABLE_NAME, null, null);
		  //dbHelper.onCreate(database);
		  
	  }
	  public ConfDet cursorToConf(Cursor cursor)
	  {
		  int id = (int) cursor.getLong(0);
		  String key = cursor.getString(1);
		  String value = cursor.getString(2);
		  ConfDet pr = new ConfDet(id,key,value);
		  return pr;
	  }
}