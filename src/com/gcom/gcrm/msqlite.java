package com.gcom.gcrm;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class msqlite extends SQLiteOpenHelper
{
	static final String PRODUCT_TABLE_NAME = "products";
	static final String CONF_TABLE_NAME = "conf";
	static final String OREDER_TABLE_NAME = "sefaresh";
	static final String OREDER_DET_TABLE_NAME = "sefareshdet";
	//create table products (id integer primary key,name text);
	private static final String PRODUCT_TABLE_CREATE = "create table " + PRODUCT_TABLE_NAME + " (id integer,name text,pic text);";
	private static final String CONF_TABLE_CREATE = "create table " + CONF_TABLE_NAME + " (id integer primary key,cKey text,cValue text);";
	private static final String ORDER_TABLE_CREATE = "create table " + OREDER_TABLE_NAME + " (id integer primary key,tarikh text);";
	private static final String ORDER_DET_TABLE_CREATE = "create table " + OREDER_DET_TABLE_NAME + " (id integer primary key,order_id integer,product_id integer,tedad integer);";

	
	private static final String DATABASE_NAME = "products.db";
	private static final int DATABASE_VERSION = 1;
	public msqlite(Context context) {
		super(context,DATABASE_NAME,null,DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(PRODUCT_TABLE_CREATE);
		db.execSQL(CONF_TABLE_CREATE);
		db.execSQL(ORDER_TABLE_CREATE);
		db.execSQL(ORDER_DET_TABLE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

}
