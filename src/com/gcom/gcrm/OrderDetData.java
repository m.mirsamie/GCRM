package com.gcom.gcrm;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

public class OrderDetData {
	 private Context mContext;
	  private SQLiteDatabase database;
	  private msqlite dbHelper;
	  private String[] allColumns = {"id","order_id","product_id","tedad"};
	  public OrderDetData(Context context)
	  {
		  this.mContext = context;
		  dbHelper = new msqlite(context);
	  }
	  public void open() throws SQLException {
		  try
		  {
			  database = dbHelper.getWritableDatabase();
		  }catch (Exception e) {
			// TODO: handle exception
			  Toast.makeText(this.mContext, e.getMessage(), Toast.LENGTH_LONG).show();
		  }
	  }
	  public void close() {
		  dbHelper.close();
	  }
	  public int addOrder(int order_id,int product_id,int tedad)
	  {
		  ContentValues values = new ContentValues();
		  int id = getLastId()+1;
		  values.put("id", id);
		  values.put("order_id",order_id);
		  values.put("product_id",product_id);
		  values.put("tedad",tedad);
		  database.insert(msqlite.OREDER_DET_TABLE_NAME, null,values);
		  return(id);
	  }
	  public int getLastId()
	  {
		  Cursor cur = database.rawQuery("select MAX(id) lid from order_det", null);
		  int lid;
		  if (cur.moveToFirst()) 
			    lid = cur.getInt(cur.getColumnIndex("lid"));
		  else 
				lid=0;
		  return(lid);
	  }
	  public List<OrderDet> getAll()
	  {
		  List<OrderDet> out = new ArrayList<OrderDet>();
		  try {
				Cursor cursor = database.query(msqlite.OREDER_DET_TABLE_NAME,allColumns, null, null, null, null, "id");
				cursor.moveToFirst();
				while (!cursor.isAfterLast()) {
					OrderDet newpr = cursorToOrderDet(cursor);
					out.add(newpr);
					cursor.moveToNext();
				}
				cursor.close();
			} catch (Exception e) {
				Toast.makeText(mContext, "DB error "+e.getMessage(), Toast.LENGTH_SHORT).show();
			}
		  return(out);
	  }
	  public List<OrderDet> getAll(int order_id)
	  {
		  List<OrderDet> out = new ArrayList<OrderDet>();
		  try {
					Cursor cursor = database.rawQuery("select * from "+msqlite.OREDER_DET_TABLE_NAME+" where order_id="+order_id,null);
					if(cursor.moveToFirst())
					{
						while (cursor.moveToNext()) {
							OrderDet newpr = cursorToOrderDet(cursor);
							out.add(newpr);
							cursor.moveToNext();
						}
						cursor.close();
					}
					else
						Toast.makeText(mContext, "no result ", Toast.LENGTH_SHORT).show();
				} catch (Exception e) {
				Toast.makeText(mContext, "DB error "+e.getMessage(), Toast.LENGTH_SHORT).show();
			}
		  return(out);
	  }
	  public OrderDet cursorToOrderDet(Cursor cursor)
	  {
		  int id = (int) cursor.getLong(0);
		  int order_id = cursor.getInt(1);
		  int product_id = cursor.getInt(2);
		  int tedad = cursor.getInt(3);
		  OrderDet ord = new OrderDet(id,order_id,product_id,tedad);
		  return ord;
	  }
}
