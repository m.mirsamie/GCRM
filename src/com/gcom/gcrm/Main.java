package com.gcom.gcrm;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

public class Main extends Activity {
	// declare the dialog as a member field of your activity
	//ProgressDialog mProgressDialog;
	WebView mywebview;
	public static String newVersion = "";
	public static int currentVersion = 0;
	

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);	
		mywebview = (WebView) findViewById(R.id.webview);
		WebSettings webSettings = mywebview.getSettings();
		mywebview.addJavascriptInterface(new WebAppInterface(this), "Android");
		webSettings.setJavaScriptEnabled(true);
		mywebview.loadUrl("file:///android_asset/main.html");
		if(android.os.Build.VERSION.SDK_INT==Build.VERSION_CODES.JELLY_BEAN)
			fixPro();
		try {
			PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			currentVersion = pInfo.versionCode;
		} catch (NameNotFoundException e) {
		}
		Conf conf=new Conf(this);
		conf.open();
		ConfDet cd = conf.getConf("updateLink");
		String updateLink = (cd.id>0)?cd.value:"http://gandom.gcom.ir/update";
		getUpdate(updateLink);
	}

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
	public void fixPro()
	{
		mywebview.getSettings().setAllowUniversalAccessFromFileURLs(true);
		mywebview.getSettings().setAllowFileAccessFromFileURLs(true);
	}
		
	public void getUpdate(String updateLink)
	{
		final DownloadTask downloadTask = new DownloadTask(this);
		downloadTask.execute(updateLink);
	}
	

	
	private class DownloadTask extends AsyncTask<String, Integer, String> {

	    private Context context;

	    public DownloadTask(Context context) {
	        this.context = context;
	    }

	    @SuppressLint({ "SdCardPath", "Wakelock" })
		@Override
	    protected String doInBackground(String... sUrl) {
	    	File ufile = new File("/sdcard/GCRM_update.apk");
	    	ufile.delete();
	        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
	        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
	             getClass().getName());
	        wl.acquire();
	        try {
	            InputStream input = null;
	            OutputStream output = null;
	            HttpURLConnection connection = null;
	            try {
	            	newVersion = "";
	                URL url = new URL(sUrl[0]+"/version.txt");
	                String fileName = "";
	                int versionNumber = 0;
	                connection = (HttpURLConnection) url.openConnection();
	                connection.connect();
	                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK)
	                     return "Server returned HTTP " + connection.getResponseCode() 
	                         + " " + connection.getResponseMessage();
	                input = connection.getInputStream();
	                BufferedReader in = new BufferedReader(new InputStreamReader(input));
	                String line = null;
	                String tmpStr = "";
	                while ((line = in.readLine()) != null) {
	                	tmpStr += line;
	                }
        			fileName = null;
	                if(!tmpStr.equals(""))
	                {
	            		String[] tmp = tmpStr.split(",");
	                	if(tmp.length == 2)
	                	{
	                		versionNumber = Integer.valueOf(tmp[0]);
	                		newVersion = String.valueOf(versionNumber);
	                		if(versionNumber > currentVersion)
	                			fileName = tmp[1];
	                	}
	                }
	                in.close();
	                if(fileName != null)
	                {
		                url = new URL(sUrl[0]+"/"+fileName);
		                connection = (HttpURLConnection) url.openConnection();
		                connection.connect();
		                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK)
		                     return "Server returned HTTP " + connection.getResponseCode() 
		                         + " " + connection.getResponseMessage();
		                int count;
		                byte data[] = new byte[4096];
		                input = connection.getInputStream();
		                output = new FileOutputStream("/sdcard/GCRM_update.apk");
	
		                while ((count = input.read(data)) != -1) {
		                    if (isCancelled())
		                        return null;
		                    output.write(data, 0, count);
		                }
		                //newVersion = String.valueOf(versionNumber);
	                }
	                else
	                	return "Last version";
	            } catch (Exception e) {
	                return e.toString();
	            } finally {
	                try {
	                    if (output != null)
	                        output.close();
	                    if (input != null)
	                        input.close();
	                } 
	                catch (IOException ignored) { }

	                if (connection != null)
	                    connection.disconnect();
	            }
	        } finally {
	            wl.release();
	        }
	        return null;
	    }
	    @Override
	    protected void onPostExecute(String result) {
	        if (result == null)
	            Toast.makeText(context,"نسخه"+newVersion+" جهت نصب آماده است. برای نصب در قسمت امکانات برروی «نصب جدید» کلیک نمایید", Toast.LENGTH_SHORT).show();
	    }
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
		mywebview.loadUrl("javascript:backPressed();");
	}
}