package com.gcom.gcrm;

public class OrderDet {
	public int id=-1;
	public int order_id=-1;
	public int product_id=-1;
	public int tedad=0;
	public OrderDet(int id,int order_id,int product_id,int tedad)
	{
		this.id = id;
		this.order_id = order_id;
		this.product_id = product_id;
		this.tedad = tedad;
	}
	public String toJSON()
	{
		String out ="{\"id\":\""+String.valueOf(this.id)+"\",\"order_id\":\""+String.valueOf(this.order_id)+"\",\"product_id\":\""+String.valueOf(this.product_id)+"\",\"tedad\":\""+String.valueOf(this.tedad)+"\"}";
		return(out);
	}
}
